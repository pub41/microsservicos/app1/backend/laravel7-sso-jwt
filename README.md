# Laravel 7 - Single Sign On - Json Web Token
## Orientações gerais quanto a instalação do JWT:
https://jwt-auth.readthedocs.io/en/docs/

# resolvendo problema de CORS:
- composer require fruitcake/laravel-cors
- app/Http/Kernel.php:
```
protected $middleware = [
  \Fruitcake\Cors\HandleCors::class,
    // ...
];
```
- php artisan vendor:publish --tag="cors"

Referência:
https://github.com/fruitcake/laravel-cors