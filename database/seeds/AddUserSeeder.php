<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AddUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name' => 'Marcelo Viana',
                'email' => 'marceloviana@infsite.org',
                'password' => '$2y$10$1En3BsHNGLTfnVXxKQTi2.SY.0N5hUp7pBqzmaGN2al3BjVkIkQBu'
            ],
            [
                'name' => 'Usuário SSO',
                'email' => 'sso@local.lan',
                'password' => '$2y$10$1En3BsHNGLTfnVXxKQTi2.SY.0N5hUp7pBqzmaGN2al3BjVkIkQBu'
            ],            
        ];

        foreach ($users as $user) {
            DB::table('users')
                ->updateOrInsert([
                    'name' => $user['name'],
                    'email' => $user['email'],
                    'password' => $user['password'],
                ]);
        }
        
    }
}
