<?php
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
    Rotas autenticadas
*/
Route::group([
    'middleware' => 'auth:api',
    'namespace' => '\App\Http\Controllers',
    'prefix' => 'auth'
], function ($router) {
    Route::post('logout', 'AuthController@logout'); // Espera o 'Bearer Token' no header da requisição
    Route::post('refresh', 'AuthController@refresh'); 
    Route::post('me', 'AuthController@me');  // Espera o 'Bearer Token' no header da requisição
    Route::get('checktoken', function () {
        response()->json(['mensagem' => 'Token válido!']);
    });
});

/*
    Rotas NÃO autenticadas
*/
Route::group([
    'prefix' => 'auth',
    'namespace' => '\App\Http\Controllers',
], function ($router) {
    Route::post('login', 'AuthController@login');  // Espera 'dados serializados' de email e password
});

Route::get('notAuthenticated', function () {
    return response()->json(['data' => 'Não autenticado!'], 401);
})->name('notAuthenticated');